(function() {
    'use strict';

    angular
        .module('app')
        .config(configure)
        .controller('appCtrl', function($scope) {});

    configure.$inject =
        ['$locationProvider', '$httpProvider', '$routeProvider', 'nywtonChessboardConfigProvider'];

    function configure ($locationProvider, $httpProvider, $routeProvider, chessboardProvider) {
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.headers.common = 'Content-Type: application/json';
        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        $routeProvider.when("/", {
            template: '<chess data-game="game" data-board="boardA" orientation="\'black\'" position="\'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1\'"></chess>',
            // template: '<chess data-game="game" data-board="boardA" orientation="\'white\'" position="\'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1\'"></chess>',
            controller: 'HomeController',
            reloadOnSearch: false
        });

        $routeProvider.otherwise({redirectTo: '/'});
        chessboardProvider.pieceTheme('../test/images/chesspieces/wikipedia/{piece}.png');
    }

})();