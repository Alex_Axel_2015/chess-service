(function () {
    'use strict';

    function ChessController($scope, $window, ChessService) {
        var game;

        $scope.$watch('$ctrl.position', function () {
            game = $scope.game = new $window.Chess($scope.$ctrl.position);
            game.name = $scope.name || 'game' + $scope.$id;
        }, true);

        $scope.init = init;
        $scope.init();

        ///////////////////////////////////////////

        function init() {

            $scope.board = function boardF() {
                return $scope.ctrl.board;
            };

            $scope.onDragStart = function onDragStartF(source, piece, position, orientation) {
                return ChessService.onDragStart($scope.game, source, piece, position, orientation);
            };
            $scope.onSnapEnd = function onSnapEndF(source, target, piece) {
                console.log($scope.game.fen());
                return ChessService.onSnapEnd($scope.game, $scope.board, source, target, piece);
            };
            $scope.onDrop = function onDropF(source, target) {
                return ChessService.onDrop($scope.game, source, target);
            };
            $scope.onChange = function onChangeF(oldPosition, newPosition) {
                return angular.noop(oldPosition, newPosition);
            };
        }
    }

    angular
        .module('app')
        .component('chess', {
            template: ['$element', '$attrs', function ($element, $attrs) {
                return '<div><nywton-chessboard board="board" data-orientation="' + $attrs.orientation + '" position="' + $attrs.position + '" draggable="true" on-change="onChange" on-drag-start-cb="onDragStart" on-snap-end="onSnapEnd" on-drop="onDrop"></nywton-chessboard></div>';
            }],
            controller: ['$scope', '$window', 'ChessService', ChessController],
            bindings: {
                'name': '@',
                'game': '=',
                'board': '=',
                'position': '='
            }
        });

})();