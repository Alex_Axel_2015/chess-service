(function() {
    'use strict';
    
    angular
        .module('app', [
            'ngRoute',
            'chess.service',
            'app.home'
        ]);

})();