var gulp = require('gulp'),
    inject = require('gulp-inject'),
    concat = require('gulp-concat'),
    replace = require('gulp-replace'),
    ngAnnotate = require('gulp-ng-annotate'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin');

gulp.task('compress-images', function () {
    return gulp.src([
        'bower_components/chessboard.js/dist/img/**/*'
    ])
        .pipe(imagemin({ progressive: true, optimizationLevel: 10 }))
        .pipe(gulp.dest('./test/images'));
});

gulp.task('index', function () {
    var target = gulp.src('./test/index.html');
    var sources = gulp.src(['./test/styles.css', './test/libs.js', './dist/chessjs.min.js', './test/app.js'], { read: false });

    return target.pipe(inject(sources))
        .pipe(replace('/test/', '../test/'))
        .pipe(replace('/dist/', '../dist/'))
        .pipe(gulp.dest('./test'));
});

gulp.task('links', function () {
    return gulp.src([
        'bower_components/chessboard.js/dist/css/chessboard.css'
    ])
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('test'));
});

gulp.task('scripts', function () {
    return gulp.src([
        'bower_components/angular/angular.min.js',
        'node_modules/angular-route/angular-route.min.js',
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/chessboard.js/dist/js/chessboard.min.js',
        'bower_components/chess.js/chess.min.js',
        'bower_components/angular-chessboard/dist/angular-chessboard.min.js'
    ])
        .pipe(concat('libs.js'))
        .pipe(gulp.dest('test'));
});

gulp.task('releaze', function () {
    return gulp.src([
        'src/chess-js.js'
    ])
        .pipe(concat('chessjs.min.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});

gulp.task('test', function () {
    return gulp.src([
        'test/app/app.module.js',
        'test/app/app.config.js',
        'test/app/home.js',
        'test/app/chess.js'
    ])
        .pipe(concat('app.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest('test'));
});

