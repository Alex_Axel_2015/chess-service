(function () {
    'use strict';

    angular
        .module('chess.service', ['nywton.chessboard'])
        .service('ChessService', chessJS);

    chessJS.$inject = [];

    function chessJS() {
        this.onDragStart = function (game, source, piece, position, orientation) {
            if (game.game_over() === true ||
                (game.turn() === 'w' && piece.search(/^b/) !== -1) ||
                (game.turn() === 'b' && piece.search(/^w/) !== -1)) {
                return false;
            }
        };

        this.onDrop = function (game, source, target) {
            var move = game.move({
                from: source,
                to: target,
                promotion: 'q'
            });

            if (move === null) return 'snapback';
        };

        this.onSnapEnd = function (game, board, source, target, piece) {
            board.position(game.fen());
        };
    }

})();